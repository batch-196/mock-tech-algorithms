let collection = [];
// Write the queue functions below.
let firstElem = 0;
let lastElem = 0;

function enqueue(element) {
    collection[lastElem] = element;
    lastElem++;
    return collection;
}

function print() {
    return collection;
}


function dequeue() {
    if (collection.length > 0) {
        for (let i = 0; i < collection.length - 1; i++) {
            collection[i] = collection[i + 1];
          };
          lastElem--;
          --collection.length;
    }

    return collection;
}

function front() {
    return collection[firstElem];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length == 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
